package calculus;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculusController {
    @RequestMapping("/calculus")
    public ResponseEntity<?> processInput(@RequestParam(value="query") String input) {
    	FullCalculator calc = new FullCalculator();
    	return calc.processInput(input);
    }
}
