package calculus;

public class ErrorResponse{

    private final boolean error;
    private final String message;
    
    public ErrorResponse (boolean error, String message) {
    	this.error = error;
    	this.message = message;
    }
    
    public boolean getError() {
        return error;
    }
    
    public String getMessage() {
        return message;
    }
    
}
