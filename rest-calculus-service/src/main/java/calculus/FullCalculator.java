package calculus;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
@Component
public class FullCalculator {
	private TokenStack operatorStack;
    private TokenStack valueStack;
    private boolean error;
    
    public FullCalculator() {
        operatorStack = new TokenStack();
        valueStack = new TokenStack();
        error = false;
    }

    private boolean processOperator(Token t) {
        Token A = null, B = null;
        if (valueStack.isEmpty()) {
        	return false;
        }
         else {
            B = valueStack.top();
            valueStack.pop();
        }
        if (valueStack.isEmpty()) {
        	return false;
        }
            
         else {
            A = valueStack.top();
            valueStack.pop();
        }
        Token R = t.operate(A.getValue(), B.getValue());
        valueStack.push(R);
        return true;
    }

    public ResponseEntity<?> processInput(String input) {
    	byte[] valueDecoded = Base64.decodeBase64(input);
    	input = new String(valueDecoded);
    	input = input.replaceAll("\\s","");
		if(StringUtils.isEmpty(input)) {
    		return formulateResponse(true, "empty query", HttpStatus.BAD_REQUEST);
    	}
    	boolean validNum = true;
    	double res = 0;
        try {
        	res = Double.parseDouble(input);
        } catch (NumberFormatException e) {
        	validNum = false;
        }
        if(validNum) {
    		return formulateResponse(false, Double.toString(res), HttpStatus.OK);
        }
	
        char[] arr = input.toCharArray();
        StringBuilder sb = new StringBuilder();
        int status = prepareTokens(arr, sb);
        if(status == -1)
        	return formulateResponse(true, "Invalid token", HttpStatus.BAD_REQUEST);
        input = sb.toString();
    	
        // The tokens that make up the input
        return doCalculus(input);
    }

	private ResponseEntity<?> doCalculus(String input) {
		String[] parts = input.split(" ");
        Token[] tokens = new Token[parts.length];
        for (int n = 0; n < parts.length; n++) {
            tokens[n] = new Token(parts[n]);
        }
        // Main loop - process all input tokens
        for (int n = 0; n < tokens.length; n++) {
            Token nextToken = tokens[n];
            if (nextToken.getType() == Token.NUMBER) {
                valueStack.push(nextToken);
            } else if (nextToken.getType() == Token.OPERATOR) {
                if (operatorStack.isEmpty() || nextToken.getPrecedence() > operatorStack.top().getPrecedence()) {
                    operatorStack.push(nextToken);
                } else {
                    while (!operatorStack.isEmpty() && nextToken.getPrecedence() <= operatorStack.top().getPrecedence()) {
                        Token toProcess = operatorStack.top();
                        operatorStack.pop();
                       if(!processOperator(toProcess)) {
                    	   return formulateResponse(true, "Invalid expression", HttpStatus.BAD_REQUEST);
                       }
                    }
                    operatorStack.push(nextToken);
                }
            } else if (nextToken.getType() == Token.LEFT_PARENTHESIS) {
                operatorStack.push(nextToken);
            } else if (nextToken.getType() == Token.RIGHT_PARENTHESIS) {
            	int leftParenthesis = 0;
                while (!operatorStack.isEmpty() && operatorStack.top().getType() == Token.OPERATOR) {
                    Token toProcess = operatorStack.top();
                    operatorStack.pop();
                    if(!processOperator(toProcess))
                    	return formulateResponse(true, "Invalid expression", HttpStatus.BAD_REQUEST);
                }
                if (!operatorStack.isEmpty() && operatorStack.top().getType() == Token.LEFT_PARENTHESIS) {
                    operatorStack.pop();
                    leftParenthesis += 1;
                    if(leftParenthesis > 1) 
                    	return formulateResponse(true, "unbalanced parenthesis.", HttpStatus.BAD_REQUEST);
                } else 
                	return formulateResponse(true, "unbalanced parenthesis.", HttpStatus.BAD_REQUEST);
            }
        }
        // Empty out the operator stack at the end of the input
        while (!operatorStack.isEmpty() && operatorStack.top().getType() == Token.OPERATOR) {
            Token toProcess = operatorStack.top();
            operatorStack.pop();
            if(!processOperator(toProcess)) 
            	return formulateResponse(true, "Invalid expression", HttpStatus.BAD_REQUEST);
        }
        
        if(!operatorStack.isEmpty() && operatorStack.top().getType() == Token.LEFT_PARENTHESIS) {
        	return formulateResponse(true, "unbalanced parenthesis.", HttpStatus.BAD_REQUEST);
        }
        // Print the result if no error has been seen.
        if(error == false) {
            Token result = valueStack.top();
            valueStack.pop();
            if (!operatorStack.isEmpty() || !valueStack.isEmpty()) 
                return formulateResponse(true, "Invalid expression", HttpStatus.BAD_REQUEST);
             else 
            	 return formulateResponse(false, Double.toString(result.getValue()), HttpStatus.OK);
        }
        return ResponseEntity.badRequest().body("Unknown");
	}

	private ResponseEntity<?> formulateResponse(boolean error, String value, HttpStatus status) {
		if(error = true)
			return new ResponseEntity<ErrorResponse>(new ErrorResponse(error, value), status);
		else
			return new ResponseEntity<ResultResponse>(new ResultResponse(error, value), status);
	}

	private int prepareTokens(char[] arr, StringBuilder sb) {
		for(int k= 0; k<arr.length; k++) {
        	if (!Character.isDigit(arr[k]) && 
        			arr[k] != '+' && arr[k] != '-' && 
        			arr[k] != '*' && arr[k] != '/' && 
        			arr[k] != '(' && arr[k] != ')') {
        		return -1;
        	}
        	//if not digit append " "	
            if(k==0) {
            if (!Character.isDigit(arr[k])) {
                sb.append(arr[k]);
                sb.append(" ");
              }
            else if (!Character.isDigit(arr[k+1])) {
                sb.append(arr[k]);
                sb.append(" ");
               }
            else {
                sb.append(arr[k]);
            }
            continue;
            }
            if(k <= arr.length -2) {
            //if the next char(k+1) is not a digit append space
            if (Character.isDigit(arr[k]) && !Character.isDigit(arr[k+1])) {
            	sb.append(arr[k]);
                sb.append(" ");
            }
            else if (!Character.isDigit(arr[k])){
                sb.append(arr[k]);
            	sb.append(" ");
            }
            else {
                sb.append(arr[k]);
            }
            }
            if(k == arr.length-1)
                sb.append(arr[k]);
        }
		return 0;
	}
}
