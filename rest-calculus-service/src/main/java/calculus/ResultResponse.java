package calculus;

public class ResultResponse{

    private final boolean error;
    private final String result;
    
    public ResultResponse (boolean error, String result) {
    	this.error = error;
    	this.result = result;
    }
    
    public boolean getError() {
        return error;
    }
    
    public String getResult() {
        return result;
    }
    
}
