package calculus;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CalculusControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void noParamCalculusShouldReturnErrorMessage() throws Exception {
        this.mockMvc.perform(get("/calculus")).andDo(print()).andExpect(status().is4xxClientError());
    }
    
    @Test
    public void calculusShouldReturnTailoredMessage() throws Exception {
    	//2+3 in base64 is Misz
        this.mockMvc.perform(get("/calculus").param("query", "Misz"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value("5.0"));
    }
    
    @Test
    public void calculusWithUnequalParanthesisShouldReturnUnbalancedParenthesisMessage() throws Exception {
    	//(2*4)+3) in base64 is KDIqNCkrMyk=
        this.mockMvc.perform(get("/calculus").param("query", "KDIqNCkrMyk="))
                .andDo(print()).andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.message").value("unbalanced parenthesis."));
    }

    @Test
    public void calculusShouldFollowMathematicalOperatorPrecedenceAndReturnResult() throws Exception {
    	//((2*4)-5+4/2) in base64 is KCgyKjQpLTUrNC8yKQ==
        this.mockMvc.perform(get("/calculus").param("query", "KCgyKjQpLTUrNC8yKQ=="))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value("5.0"));
    }
}
